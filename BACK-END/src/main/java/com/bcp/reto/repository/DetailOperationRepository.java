package com.bcp.reto.repository;

import com.bcp.reto.model.DetailOperationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailOperationRepository extends CrudRepository<DetailOperationEntity, Long> {

}
