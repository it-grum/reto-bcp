package com.bcp.reto.repository;

import com.bcp.reto.model.ExchangeRateEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeRateRepository extends CrudRepository<ExchangeRateEntity, Long> {

  @Query(value = "select * from exchange_rate where currency_id = :id and "
       + "enabled = :value limit 1", nativeQuery = true)
  ExchangeRateEntity findByCurrencyId(@Param("id") Long id,
                                      @Param("value") Boolean value);

  @Query(value = "select * from exchange_rate er join currency c on"
      + " er.currency_id = c.id where er.enabled = true", nativeQuery = true)
  Iterable<ExchangeRateEntity> findByEnabledTrue();
}
