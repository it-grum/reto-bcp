package com.bcp.reto.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;

@Entity
@Table(name = "detailOperation")
@Data
@Builder
public class DetailOperationEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long IdDetailOperation;

  @Column(name = "amount")
  private Double amount;

  @Column(name = "origin_currency_id")
  private Long originCurrencyId;

  @Column(name = "destination_currency_id")
  private Long destinationCurrencyId;

  @Column(name = "exchange_rate_id")
  private Long exchangeRateId;

  @Column(name = "date_operation", insertable = false)
  private Date dateOperation;

}
