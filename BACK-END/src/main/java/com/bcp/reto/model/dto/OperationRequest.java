package com.bcp.reto.model.dto;

import java.io.Serializable;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OperationRequest implements Serializable {

  private static final long serialVersionUID = UUID.randomUUID().hashCode();

  private Long originCurrencyId;
  private String originCurrencyDescription;
  private Long destinationCurrencyId;
  private String destinationCurrencyDescription;
  private Double amount;

}
