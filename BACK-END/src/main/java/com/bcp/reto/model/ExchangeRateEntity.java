package com.bcp.reto.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "exchangeRate")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long exchangeRateId;

  @Column(name = "exchange_rate")
  private Double exchangeRate;

  @Column(name = "enabled")
  private Boolean enabled;

  @Column(name = "date_operation", insertable = false, updatable = false)
  @CreationTimestamp
  private Date dateOperation;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "currency_id", referencedColumnName = "id")
  private CurrencyEntity currency;

}
