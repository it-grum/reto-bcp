package com.bcp.reto.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class OperationResponse {

  private Double amount;
  private Double amountWithExchangeRate;
  private String originCurrencyDescription;
  private String destinationCurrencyDescription;
  private Double exchangeRate;

}
