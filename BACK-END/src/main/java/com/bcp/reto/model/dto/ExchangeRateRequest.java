package com.bcp.reto.model.dto;

import java.io.Serializable;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class ExchangeRateRequest implements Serializable {

  private static final long serialVersionUID = UUID.randomUUID().hashCode();

  private Long currencyId;
  private Long exchangeRateId;
  private Double exchangeRate;
  private Double exchangeRateNew;
  private String currencyCode;

}
