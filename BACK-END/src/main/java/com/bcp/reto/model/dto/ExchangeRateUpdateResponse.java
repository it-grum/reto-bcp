package com.bcp.reto.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder(toBuilder = true)
@ToString
public class ExchangeRateUpdateResponse {

  private String status;

}
