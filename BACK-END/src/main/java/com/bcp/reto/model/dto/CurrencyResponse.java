package com.bcp.reto.model.dto;

import java.io.Serializable;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder(toBuilder = true)
@ToString
public class CurrencyResponse implements Serializable {

  private static final long serialVersionUID = UUID.randomUUID().hashCode();

  private Long id;
  private String currencyCode;
  private String currencyDescription;

}
