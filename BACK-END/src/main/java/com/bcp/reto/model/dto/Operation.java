package com.bcp.reto.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase auxiliar que almacena los datos para:
 * - realizar los cálculos del tipo de cambio
 * - persistir el detalle de la operación
 * - retonar los datos de la operación calculada
 */
@Getter
@Setter
@Builder
public class Operation {

  private Double amount;
  private Long originCurrencyId;
  private Long destinationCurrencyId;
  private Long originExchangeRateId;
  private Long destinationExchangeRateId;
  private Double originExchangeRateAmount;
  private String originExchangeRateDescription;
  private Double destinationExchangeRateAmount;
  private String destinationExchangeRateDescription;
  private Double amountWithExchangeRate;

}
