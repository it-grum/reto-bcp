package com.bcp.reto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity(name = "currency")
@Table(name = "currency")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CurrencyEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long currencyId;

  @Column(name = "currency_code")
  private String currencyCode;

  @Column(name = "currency_description")
  private String currencyDescription;

}
