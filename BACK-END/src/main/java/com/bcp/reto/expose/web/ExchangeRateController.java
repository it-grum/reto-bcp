package com.bcp.reto.expose.web;

import com.bcp.reto.model.dto.CurrencyResponse;
import com.bcp.reto.model.dto.ExchangeRateRequest;
import com.bcp.reto.model.dto.ExchangeRateResponse;
import com.bcp.reto.model.dto.ExchangeRateUpdateResponse;
import com.bcp.reto.model.dto.OperationRequest;
import com.bcp.reto.model.dto.OperationResponse;
import com.bcp.reto.service.ExchangeRateService;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/v1/exchange")
@CrossOrigin(origins = "http://localhost:4200")
public class ExchangeRateController {

  @Autowired
  private ExchangeRateService exchangeService;

  @GetMapping(value = "/currency", produces = {MediaType.APPLICATION_JSON_VALUE})
  public Observable<CurrencyResponse> getAllCurrency() {
    return exchangeService.getAllCurrency();
  }

  @PostMapping(value = "/operation", consumes = {MediaType.APPLICATION_JSON_VALUE})
  public Single<OperationResponse> setOperation(@RequestBody OperationRequest operationRequest) {
    return exchangeService.setOperation(operationRequest);
  }

  @PostMapping(value = "/exchange-rate", produces = {MediaType.APPLICATION_JSON_VALUE})
  public Single<ExchangeRateUpdateResponse> setExchangeRate(@RequestBody ExchangeRateRequest request) {
    return exchangeService.setExchangeRate(request);
  }

  @GetMapping(value = "/exchange-rate", produces = { MediaType.APPLICATION_JSON_VALUE })
  public Observable<ExchangeRateResponse> getExchangeRate() {
    return exchangeService.getExchangeRate();
  }

}
