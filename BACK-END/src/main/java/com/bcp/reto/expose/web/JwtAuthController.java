package com.bcp.reto.expose.web;

import com.bcp.reto.security.jwt.JwtRequest;
import com.bcp.reto.security.jwt.JwtResponse;
import com.bcp.reto.security.jwt.JwtTokenUtil;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class JwtAuthController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private UserDetailsService jwtInMemoryUserDetailsService;

  @PostMapping(value = "/authentication", consumes = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
      throws Exception {

    authenticate(authenticationRequest);

    final UserDetails userDetails = jwtInMemoryUserDetailsService
        .loadUserByUsername(authenticationRequest.getUsername());

    final String token = jwtTokenUtil.generateToken(userDetails);

    return ResponseEntity.ok(new JwtResponse(token));
  }

  private void authenticate(JwtRequest authenticationRequest) throws Exception {
    Optional<String> username = Optional.ofNullable(authenticationRequest.getUsername());
    Optional<String> password = Optional.ofNullable(authenticationRequest.getPassword());

    if (username.isPresent() && password.isPresent()) {
      try {
        authenticationManager
            .authenticate(new UsernamePasswordAuthenticationToken(username.get(), password.get()));
      } catch (DisabledException e) {
        throw new Exception("USER_DISABLED", e);
      } catch (BadCredentialsException e) {
        throw new Exception("INVALID_CREDENTIALS", e);
      }
    }
  }
}
