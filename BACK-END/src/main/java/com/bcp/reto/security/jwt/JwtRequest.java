package com.bcp.reto.security.jwt;

import java.io.Serializable;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JwtRequest implements Serializable {

  private static final long serialVersionUID = UUID.randomUUID().hashCode();

  private String username;
  private String password;

}
