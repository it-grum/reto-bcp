package com.bcp.reto.security.jwt;

import java.io.Serializable;
import java.util.UUID;

public class JwtResponse implements Serializable {

  private static final long serialVersionUID = UUID.randomUUID().hashCode();

  private final String jwttoken;

  public JwtResponse(String jwttoken) {
    this.jwttoken = jwttoken;
  }

  public String getToken() {
    return this.jwttoken;
  }

}
