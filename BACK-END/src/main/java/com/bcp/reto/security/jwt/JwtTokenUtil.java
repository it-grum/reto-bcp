package com.bcp.reto.security.jwt;

import io.jsonwebtoken.SignatureAlgorithm;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenUtil implements Serializable {

 private static final long SerialVersionUID = UUID.randomUUID().hashCode();

 public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

 @Value("${jwt.secret}")
  private String secret;

 public String getUsernameFromToken(String token) {
   return getClaimFromToken(token, Claims::getSubject);
 }

 public Date getExpirationDateFromToken(String token) {
   return getClaimFromToken(token, Claims::getExpiration);
 }

 public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
   final Claims claims = getAllClaimsFromToken(token);
   return claimsResolver.apply(claims);
 }

 private Claims getAllClaimsFromToken(String token) {
   return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
 }

  /**
   * Método que revisa si el token ha expirado
   * @param token
   * @return new Date
   */
 private Boolean isTokenExpired(String token) {
   final Date expiration = getExpirationDateFromToken(token);
   return expiration.before(new Date());
 }

  /**
   * Método que genera el token
   * @param userDetails
   * @return token
   */
 public String generateToken(UserDetails userDetails) {
   Map<String, Object> claims = new HashMap<>();
   return doGenerateToken(claims, userDetails.getUsername());
 }

  /**
   * Se define los claims del token, como expiration, subject, id
   * Firma el token con el algoritmo HS512 y una clave secreta
   * Compacta el JWT a una url-safe
   * @param claims
   * @param subject
   * @return token
   */
 private String doGenerateToken(Map<String, Object> claims, String subject) {
   return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
       .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
       .signWith(SignatureAlgorithm.HS512, secret).compact();
 }

  /**
   * Método que valida el token y verifica que no expiró
   * @param token
   * @param userDetails
   * @return Boolean
   */
 public Boolean validateToken(String token, UserDetails userDetails) {
   final String username = getUsernameFromToken(token);
   return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
 }

}
