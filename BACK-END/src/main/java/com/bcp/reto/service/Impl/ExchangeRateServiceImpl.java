package com.bcp.reto.service.Impl;

import com.bcp.reto.model.CurrencyEntity;
import com.bcp.reto.model.DetailOperationEntity;
import com.bcp.reto.model.ExchangeRateEntity;
import com.bcp.reto.model.dto.CurrencyResponse;
import com.bcp.reto.model.dto.ExchangeRateRequest;
import com.bcp.reto.model.dto.ExchangeRateResponse;
import com.bcp.reto.model.dto.ExchangeRateUpdateResponse;
import com.bcp.reto.model.dto.Operation;
import com.bcp.reto.model.dto.OperationRequest;
import com.bcp.reto.model.dto.OperationResponse;
import com.bcp.reto.repository.CurrencyRepository;
import com.bcp.reto.repository.DetailOperationRepository;
import com.bcp.reto.repository.ExchangeRateRepository;
import com.bcp.reto.service.ExchangeRateService;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ExchangeRateServiceImpl implements ExchangeRateService {

  @Autowired
  private ExchangeRateRepository exchangeRateRepository;

  @Autowired
  private CurrencyRepository currencyRepository;

  @Autowired
  private DetailOperationRepository detailOperationRepository;

  /**
   * Lógica de negocio para la construcción de la operación cambiaria
   *
   * @param request
   * @return Single<OperationResponse>
   */
  public Single<OperationResponse> setOperation(OperationRequest request) {
    return Single.just(getByCurrencyId(request.getOriginCurrencyId(), Boolean.TRUE))
        .filter(filterOrigin -> Optional.ofNullable(filterOrigin).isPresent())
        .map(Optional::get)
        .flatMapSingle(rateOrigin -> {
              log.info("caso1:: id origen existe");
              var operation = getByCurrencyId(request.getDestinationCurrencyId(), Boolean.TRUE)
                  .filter(filterDestination -> Optional.ofNullable(filterDestination).isPresent())
                  .map(rateDestination -> {
                    log.info("caso2:: id destino existe");
                    return buildOperation(request, rateOrigin, rateDestination);
                  });
              log.info("operation::::{}", operation);
              return buildOperationResponse(operation);
            }
        ).onErrorResumeNext(Single.just(OperationResponse.builder().build()));
  }

  /**
   * Método que calcula el tipo de cambio
   * A: monto en una moneda
   * B: tasa de conversión
   * C: dinero que
   * recibirá C = A * B
   *
   * @return C
   */
  private Double calculate(Double amount, Double destinationExchangeRate) {
    /*
    cambiar de sol a dolar
    sol - dolar
     1     0.24
     10   x
     (10 * .24) / 1 = 2.4 dolares
     */
    /*
      cambiar de dolar a sol
      dolar - sol
      1       4
      10
      (10 * 4) / 1 = 40 soles
     */
    return amount * destinationExchangeRate;
  }

  /**
   * Guarda el detalle de la operación realizada
   *
   * @param operation
   */
  private void saveOperation(Operation operation) {
    DetailOperationEntity detailOperationEntity =
        DetailOperationEntity.builder()
            .amount(operation.getAmount())
            .originCurrencyId(operation.getOriginCurrencyId())
            .destinationCurrencyId(operation.getDestinationCurrencyId())
            .exchangeRateId(operation.getDestinationExchangeRateId())
            .build();

    detailOperationRepository.save(detailOperationEntity);
  }

  /**
   * Método que obtiene el tipo de cambio con estado activo
   *
   * @param id, value
   * @return Optional<ExchangeRateEntity>
   */
  private Optional<ExchangeRateEntity> getByCurrencyId(Long id, Boolean value) {
    return Optional.ofNullable(exchangeRateRepository.findByCurrencyId(id, value));
  }

  /**
   * Método que obtiene todas las monedas cambiarias
   *
   * @return CurrencyResponse
   */
  @Override
  public Observable<CurrencyResponse> getAllCurrency() {
    return Observable.fromIterable(currencyRepository.findAll())
        .doOnNext(currencyEntities -> log.info("currency::: {}", currencyEntities))
        .map(currencyEntity -> buildCurrencyResponse(currencyEntity));
  }

  /**
   * Método que obtiene todos las tipos de cambio en estado activo
   *
   * @return Observable<ExchangeRateResponse>
   */
  @Override
  public Observable<ExchangeRateResponse> getExchangeRate() {
    return Observable.fromIterable(exchangeRateRepository.findByEnabledTrue())
        .map(exchangeRateEntities -> buildExchangeRateResponse(exchangeRateEntities));
  }

  @Override
  public Single<ExchangeRateUpdateResponse> setExchangeRate(ExchangeRateRequest request) {

    return Single.just(Optional.ofNullable(exchangeRateRepository.findById(request.getExchangeRateId())))
        .filter(exchangeRateEntity -> Optional.ofNullable(exchangeRateEntity).isPresent())
        .map(Optional::get)
        .flatMapSingle(exchangeRateEntity -> {
          var ex = ExchangeRateEntity.builder()
              .exchangeRateId(exchangeRateEntity.get().getExchangeRateId())
              .enabled(Boolean.TRUE)
              .exchangeRate(request.getExchangeRateNew())
              .currency(exchangeRateEntity.get().getCurrency())
              .build();
          exchangeRateRepository.save(ex);
          return Single.just(ExchangeRateUpdateResponse.builder()
              .status("registro actualizado").build());
        });
  }

  /**
   * Este método realiza lo siguiente:
   * Realiza el calculo para el tipo de cambio
   * Guarda el detalle de la operación realizada
   * Construye el objeto de respuesta de las operaciones calculadas
   *
   * @param operation
   * @return Single<OperationResponse>
   */
  private Single<OperationResponse> buildOperationResponse(Optional<Operation> operation) {
    log.info("caso3:: id de origin y destino existe");
    var calc = calculate(operation.get().getAmount(),
        operation.get().getDestinationExchangeRateAmount());

    saveOperation(operation.get());
    return Single.just(OperationResponse.builder()
        .amount(operation.get().getAmount())
        .originCurrencyDescription(operation.get().getOriginExchangeRateDescription())
        .destinationCurrencyDescription(operation.get().getDestinationExchangeRateDescription())
        .amountWithExchangeRate(calc)
        .exchangeRate(operation.get().getDestinationExchangeRateAmount())
        .build());
  }

  /**
   * Construye la información en base a los tipos de cambio y monto seleccionado por el usuario
   *
   * @param operation
   * @param origin
   * @param destination
   * @return Operation
   */
  private Operation buildOperation(OperationRequest operation, ExchangeRateEntity origin,
      ExchangeRateEntity destination) {
    return Operation.builder()
        .amount(operation.getAmount())
        .originCurrencyId(operation.getOriginCurrencyId())
        .destinationCurrencyId(operation.getDestinationCurrencyId())
        .originExchangeRateId(origin.getExchangeRateId())
        .originExchangeRateAmount(origin.getExchangeRate())
        .originExchangeRateDescription(operation.getOriginCurrencyDescription())
        .destinationExchangeRateId(destination.getExchangeRateId())
        .destinationExchangeRateAmount(destination.getExchangeRate())
        .destinationExchangeRateDescription(
            operation.getDestinationCurrencyDescription())
        .build();
  }

  /**
   * Método que construye la respuesta para el tipo de cambio
   *
   * @param exchangeRateEntity
   * @return ExchangeRateResponse
   */
  private ExchangeRateResponse buildExchangeRateResponse(ExchangeRateEntity exchangeRateEntity) {
    return ExchangeRateResponse.builder()
        .exchangeRate(exchangeRateEntity.getExchangeRate())
        .currencyId(exchangeRateEntity.getCurrency().getCurrencyId())
        .currencyCode(exchangeRateEntity.getCurrency().getCurrencyCode())
        .exchangeRateId(exchangeRateEntity.getExchangeRateId())
        .build();
  }

  /**
   * Método que construye el objeto de respuesta para las monedas
   *
   * @param currencyEntity
   * @return CurrencyResponse
   */
  private CurrencyResponse buildCurrencyResponse(CurrencyEntity currencyEntity) {
    return CurrencyResponse.builder()
        .id(currencyEntity.getCurrencyId())
        .currencyCode(currencyEntity.getCurrencyCode())
        .currencyDescription(currencyEntity.getCurrencyDescription())
        .build();
  }

}
