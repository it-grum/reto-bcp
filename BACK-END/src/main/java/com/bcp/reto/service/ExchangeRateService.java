package com.bcp.reto.service;

import com.bcp.reto.model.dto.CurrencyResponse;
import com.bcp.reto.model.dto.ExchangeRateRequest;
import com.bcp.reto.model.dto.ExchangeRateResponse;
import com.bcp.reto.model.dto.ExchangeRateUpdateResponse;
import com.bcp.reto.model.dto.OperationRequest;
import com.bcp.reto.model.dto.OperationResponse;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.springframework.stereotype.Service;

@Service
public interface ExchangeRateService {

  Single<OperationResponse> setOperation(OperationRequest operationRequest);

  Observable<CurrencyResponse> getAllCurrency();

  Observable<ExchangeRateResponse> getExchangeRate();

  Single<ExchangeRateUpdateResponse> setExchangeRate(ExchangeRateRequest request);

}
