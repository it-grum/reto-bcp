--DROP TABLE IF EXISTS currency;
--DROP TABLE IF EXISTS exchange_rate;

CREATE TABLE currency (
  id INT AUTO_INCREMENT PRIMARY KEY,
  currency_code VARCHAR(10),
  currency_description VARCHAR(50)
);

CREATE TABLE exchange_rate (
  id INT AUTO_INCREMENT PRIMARY KEY,
  exchange_rate NUMERIC(8, 2),
  date_operation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  enabled BOOLEAN,
  currency_id INT,
  FOREIGN KEY (currency_id) references currency(id)
);

CREATE TABLE detail_operation (
  id INT AUTO_INCREMENT PRIMARY KEY,
  amount NUMERIC(8, 2),
  date_operation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  origin_currency_id INT,
  destination_currency_id INT,
  exchange_rate_id INT
);

INSERT INTO currency (id, currency_code, currency_description) VALUES
  (1, 'SOL', 'Nuevo Sol'),
  (2, 'EUR', 'Euro'),
  (3, 'USR', 'dolar');

INSERT INTO exchange_rate (id, exchange_rate, date_operation, enabled, currency_id) VALUES
  (1, 1.02, '2012-09-17 18:47:52.69', 1, 1),
  (2, 1.03, '2012-09-17 18:47:52.69', 1, 2),
  (3, 1.10, '2012-09-17 18:47:52.69', 0, 1),
  (4, 1.50, '2012-09-17 18:47:52.69', 1, 3);
