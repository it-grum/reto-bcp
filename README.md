## Front End

Para levantar el proyecto considerar que se deberá descargar todas las dependencias del archivo package.json

En una consola, correr el comando npm install

si apareciese un error al momento de correr el comando ng serve --open, ejecutar el comando npm install boostrap jquery @popperjs/core

Si todo está correcto el proyecto correrá sobre el puerto 4200 -> http://localhost:4200

Cuenta con lo siguiente: 

1. Routing
2. Interceptores para las llamadas al backend
3. Integración de JWT
4. Flujo hacia el backend
5. Validaciones básicas


## Back End

Para levantar el proyecto considerar que se deberá descargar todas las dependencias del archivo pom.xml

El proyecto correrá sobre el puerto 8081

Cuenta con lo siguiente:

1. Seguridad con JWT
2. Base de datos en memoria H2
3. Integración de RxJava 2 para la lógica de negocio
4. Modelo ER en el directorio de recursos

Los endpoints a consultar son los siguientes:

GET  http://localhost:8081/api/v1/exchange/currency  -> obtiene todas las monedas para realizar el tipo de cambio
GET  http://localhost:8081/api/v1/exchange/exchange-rate -> obtiene la moneda y sus tipos de cambio
POST http://localhost:8081/api/v1/exchange/exchange-rate -> actualiza el tipo de cambio
POST http://localhost:8081/api/v1/exchange/operation -> realiza la operación de conversión de moneda
POST http://localhost:8081/api/v1/authentication -> realiza la autenticación del usuario


