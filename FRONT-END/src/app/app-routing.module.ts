import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { ExchangeRateComponent } from './scenes/exchange-rate/exchange-rate.component';
import { HomeComponent } from './scenes/home/home.component';
import { LoginComponent } from './scenes/login/login.component';
import { OperationComponent } from './scenes/operation/operation.component';

const routes: Routes = [
  { 
    path: '', 
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'operation',
    component: OperationComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'exchange-rate',
    component: ExchangeRateComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
