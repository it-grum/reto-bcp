/**
 * Clase para almacenar la moneda
 */
export class Currency {
    id: number;
    currencyCode?: string;
    currencyDescription?: string;
}
