/**
 * clase que representa al usuario
 */
export class User {
    username: string;
    password: string;
    token?: any;
}
