export class OperationDetail {
    amount: number;
    amountWithExchangeRate: number;
    destinationCurrencyDescription: string;
    exchangeRate: number;
    originCurrencyDescription: string;
}