/**
 * clase que va a enviar la operacion del tipo de cambio
 */
 export class Operation {
    amount?: number;
    originCurrencyId?: number;
    originCurrencyDescription?: string;
    destinationCurrencyId?: number;
    destinationCurrencyDescription?: string;
}
