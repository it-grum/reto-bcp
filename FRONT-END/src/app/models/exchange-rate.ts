/** 
 * Clase para almacenar los tipos de cambio
 */
export class ExchangeRate {
    currencyId: number;
    exchangeRateId: number;
    exchangeRate: number;
    exchangeRateNew: number;
    currencyCode: string;
}

export class ExchangeRateUpdate {
    status: string;
}
