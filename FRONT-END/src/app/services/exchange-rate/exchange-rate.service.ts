import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ExchangeRate, ExchangeRateUpdate } from '../../models/exchange-rate';
import { Utils } from '../utils/utils';


@Injectable({
  providedIn: 'root'
})
export class ExchangeRateService {

  apiUrl = 'http://localhost:8081/api/v1/exchange/';

  constructor(private http: HttpClient, private utils: Utils) { }

  /**
   * Método que obtiene el tipo de cambio
   * @returns ExchangeRate[]
   */
  getExchangeRate(): Observable<ExchangeRate[]> {
    return this.http.get<ExchangeRate[]>(this.apiUrl + 'exchange-rate')
    .pipe(
      catchError(this.utils.handleError)
    );
  }

  updateExchangeRate(value: ExchangeRate): Observable<ExchangeRateUpdate> {
    return this.http.post<any>(this.apiUrl + 'exchange-rate', value)
    .pipe(
      catchError(this.utils.handleError)
    );
  }

}
