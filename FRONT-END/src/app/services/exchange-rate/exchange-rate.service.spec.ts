import { TestBed } from '@angular/core/testing';

import { ExchangeRate.ServiceService } from './exchange-rate.service';

describe('ExchangeRate.ServiceService', () => {
  let service: ExchangeRate.ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExchangeRate.ServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
