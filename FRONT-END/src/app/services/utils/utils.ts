import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class Utils {
handleError(error: any) {
    let errorMessage = '';
    
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error code:  ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
