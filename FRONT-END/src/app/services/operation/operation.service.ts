import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Currency } from 'src/app/models/currency';
import { Operation } from 'src/app/models/operation';
import { OperationDetail } from 'src/app/models/operation-detail';
import { Utils } from '../utils/utils';

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  apiUrl = 'http://localhost:8081/api/v1/exchange/';

  constructor(private http: HttpClient, private utils: Utils) { }

  /**
  * Método que obtiene las monedas
  * @returns Currency[]
  */
   getCurrency(): Observable<Currency[]> {
    return this.http.get<Currency[]>(this.apiUrl + 'currency')
    .pipe(
      catchError(this.utils.handleError)
    );
  }

   /**
   * Método que calcula el tipo de cambio
   */
    operationExchangeRate2(value: Operation): any {
      console.log(JSON.stringify(value));
      return null;
    }

   operationExchangeRate(value: Operation): Observable<OperationDetail> {
    return this.http.post<any>(this.apiUrl + 'operation', value)
    .pipe(
      catchError(this.utils.handleError)
    );
  }
}
