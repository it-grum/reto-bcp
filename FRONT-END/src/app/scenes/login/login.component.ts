import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, 
              private route: ActivatedRoute,
              private router: Router) {
   }

  // valores seteados para efectos de demo
  public username: string = 'alan';
  public password: string = 'password';
  public returnUrl: string;
  public message: string;

  ngOnInit(): void { }

  login() {
    
    this.authService.login(this.username, this.password)
      .pipe(first())
      .subscribe(data => {
        this.router.navigate(['operation']);
      }, error => {
        console.log(error)
        this.message = 'Login incorrecto'
      });
  }

}
