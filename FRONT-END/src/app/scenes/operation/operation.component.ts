import { Component, OnInit } from '@angular/core';
import { Currency } from 'src/app/models/currency';
import { Operation } from 'src/app/models/operation';
import { OperationDetail } from 'src/app/models/operation-detail';
import { OperationService } from 'src/app/services/operation/operation.service';

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {

  public currency: Currency[];
  public status: any;
  public demo: any;
  public currencyOrigin: any;
  public currencyDestination: any;
  public operation: Operation = {};
  public amount: number;
  public operationDetail: OperationDetail;

  constructor(public operationService: OperationService) { }

  ngOnInit(): void {
    this.getCurrency();
  }

  reload() {
    window.location.reload();
  }

  getCurrency() {
    return this.operationService.getCurrency()
    .subscribe((data: Currency[]) => {
      this.currency = data;
    })
  }

  operationExchangeRate() {
    this.operation.amount = this.amount;
    console.log(this.operation);
    return this.operationService.operationExchangeRate(this.operation)
      .subscribe((data: any) => {
      console.log(data);
      this.operationDetail = data;
      this.status = data?.status;
      })
  }

  onChangeOrigin(value: any): void {
    this.status = null;
    this.currency.forEach(e => {
      if (+e.id == +value) {
        this.operation.originCurrencyId = e.id;
        this.operation.originCurrencyDescription = e.currencyDescription;
      }
    });
  }

  onChangeDestination(value: any): void {
    this.status = null;
    this.currency.forEach(e => {
      if (+e.id == +value) {
        this.operation.destinationCurrencyId = e.id;
        this.operation.destinationCurrencyDescription = e.currencyDescription;
      }
    });
  }

}
