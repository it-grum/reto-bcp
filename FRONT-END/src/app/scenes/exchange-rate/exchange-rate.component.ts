import { Component, OnInit } from '@angular/core';
import { ExchangeRate } from 'src/app/models/exchange-rate';
import { ExchangeRateService } from 'src/app/services/exchange-rate/exchange-rate.service';

@Component({
  selector: 'app-exchange-rate',
  templateUrl: './exchange-rate.component.html',
  styleUrls: ['./exchange-rate.component.css']
})
export class ExchangeRateComponent implements OnInit {

  constructor(public exchangeService: ExchangeRateService) { }

  public exchange: ExchangeRate[];
  public exchangeRate: any;
  public updateExchangeRateScope: ExchangeRate;
  public exchangeRateNew: any = 0;
  public status: any;
  public enabledSend: Boolean = true;

  ngOnInit(): void {
    this.getExchangeRate();
  }

  getExchangeRate() {
   return this.exchangeService.getExchangeRate()
       .subscribe((data: ExchangeRate[]) => {
          this.exchange = data;
       })
  }

  reload() {
    window.location.reload();
  }

  updateExchangeRate() {
    this.updateExchangeRateScope.exchangeRateNew = this.exchangeRateNew;
    return this.exchangeService.updateExchangeRate(this.updateExchangeRateScope)
      .subscribe((data: any) => {
      this.status = data?.status;
      this.reload();
      })
  }

  onChange(value: any): void {
    // validacion muy basica
    value === '0' ? this.enabledSend = true: this.enabledSend = false; 

    this.status = null;
    this.exchange.forEach(e => {
      if (+e.currencyId == +value) {
        this.exchangeRate = e.exchangeRate;
        this.updateExchangeRateScope = e;
      }
    });
  }
}
